import React, {memo, useEffect, useState} from "react";
import "./App.css";

const Button = memo((props) => {
  console.count("render button");
  return <button {...props} style={{ backgroundColor: "lightgray" }} />;
});

const ListItem = memo(({ children }) => {
  console.count("render list item");
  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
});

const App = memo(() => {
  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    console.count("render fetch");
    fetch("https://reqres.in/api/products")
      .then((response) => response.json())
      .then((json) =>
        setProducts(
          json.data
            .filter((item) => item.name.includes(searchString))
            .sort((a, z) =>
              isSortingDesc
                ? z.name.localeCompare(a.name)
                : a.name.localeCompare(z.name)
            )
        )
      );
  }, [searchString, isSortingDesc]);

  // trying to optimisation callback function and make them non-anonymous
  const onInputChangeHandler = (e) => setSearchString(e.target.value);
  const onButtonClickHandler = () => setSortingDesc((value) => !value);

  console.count("render app");

  return (
    <div className="App">
      <input
        type="search"
        value={searchString}
        onChange={onInputChangeHandler}
      />
      <Button onClick={onButtonClickHandler}>
        Change sort direction
      </Button>
      <ul>
        {products.map((product) => {
          return <ListItem key={product.id}>{product.name}</ListItem>;
        })}
      </ul>
    </div>
  );
});

export default App;
